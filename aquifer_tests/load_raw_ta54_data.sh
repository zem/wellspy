#!/bin/tcsh -f

if( $#argv != 1 ) then
	echo
	echo "Usage: load_raw_ta54_data.sh data_file"
	echo
	exit(0)
endif

dos2unix $1

awk 'NR>7 {print "\""$3"-"$1"-"$2" "$4":"$5":00\","$7","$8}' $1 >! $1:r-mysql.txt

cat > $$-temp << EOF
use LocalWork;
LOAD DATA LOCAL INFILE '$1:r-mysql.txt' INTO TABLE TA54BaroData FIELDS TERMINATED BY ',' ENCLOSED BY '"';
EOF

echo "\nLoading data...\n"
mysql -h aquifer -p < $$-temp

rm -f $$-temp

