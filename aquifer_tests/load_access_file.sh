#!/bin/tcsh -f
# Should have 2 arguments:
# ./load_access_file.sh data_file db_table_name
#
# data_file : is the file that contains data that needs to be load to the database
# db_table_name : the name of the table where to load this data
#
# the clean_db.bat should be in the directory

if ( $#argv < 2 ) then
	echo
	echo "Usage: load_access_file.sh data_file db_table_name"
	echo 
	echo "LocalWork tables (db_table_name):"
	echo "GroundWaterLevelData"
	echo "PortScrn"
	echo "ProdDaily"
	echo "PumpRate"
	echo "ScreenData"
	echo "WQDBLocation"
	echo
	exit(0)
endif

echo "Enter MySQL password - "
stty -echo
set PASSWORD = $< 
stty echo
echo

if ( $2 == 'ScreenData' ) then
    set UID='SCREEN_ID'
else if ( $2 == 'PortScrn' ) then
    set UID="LOCATION_NAME, PORT_ID, SCREEN_ID"
else
    set UID="ID"
endif

dos2unix $1
set now=`date +"%Y%m%d_%s"`
mysqldump -p$PASS LocalWork $2 > LocalWork_$2-$now.sql

cat > $$-temp << EOF
use LocalWork;
RENAME TABLE $2 TO temp_table;
ALTER TABLE temp_table DISABLE KEYS;
LOAD DATA LOCAL INFILE '$1' INTO TABLE temp_table FIELDS TERMINATED BY ',' ENCLOSED BY '"';
ALTER TABLE temp_table ENABLE KEYS;
CREATE TABLE $2 LIKE temp_table;
ALTER TABLE $2 ADD UNIQUE ($UID);
INSERT IGNORE INTO $2 SELECT * FROM temp_table;
DROP TABLE temp_table;
EOF

echo "\nLoading data...\n"
mysql -p$PASSWORD -h madsmax < $$-temp

rm -f $$-temp

if ( $#argv != 3 ) then
    mysql -p$PASS < clean_db.bat
endif
