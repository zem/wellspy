#!/bin/tcsh -f

if ( $#argv < 1 ) then
	echo "USAGE: epcsvtomysql.sh tab_sep_file"
	exit 0
endif

chmod -x $1
dos2unix $1 >& /dev/null

# Find beginning and end of data
#set begin = `grep -n "DATE" $1 | awk -F: 'NR==2 {print $1}'`
#set end = `grep -n "LOS ALAMOS" $1 | awk -F: 'NR==3 {print $1-1}'`
#@ end -= 3

# Create headers array with well names on first line of file
sed -n '1p' $1 > $$-header
set headers = `sed -n 1p $$-header | sed 's/GUAJE /G-/g' | sed 's/OTOWI /O-/g' | sed 's/PAJ /PM-/g' | awk '{for(i=2;i<=NF;i++) printf "%s ", $i} END {printf "\n"}'`

# Extract data
sed -n '2,$p' $1 >! $$-data

# Extract times and reformat
awk '{print $1}' $$-data | awk -F/ '{print $3"-"$1"-"$2}' > $$-times

# Extract volumes from data
cut -f2- -d '	' $$-data > $$-volumes

# Loop through each header, formatting data as csv file for input into mysql ProdDaily table
@ i = 1
foreach k ( $headers )
	awk -v v1=$i '{print $v1}' $$-volumes >! $$-tempcol
	paste $$-times $$-tempcol >! $$-temp
	awk -v v1=$k '{print "1,"$1" 0:00:00,\""v1"\",,,,"$2",,,,,"}' $$-temp
	@ i += 1
end

# Clean up
rm -f $$-*

