#!/usr/bin/env python
#The purpose of this script is to replace the *s in the baro measurements
#with interpolated values
#It also makes sure that days, months, years, hours, minutes have 2 digits.
#It should be run on a file like ta54_15_20XX...txt after the headers have
#been removed.
import sys

def isNumber(s):
	try:
		float(s)
		return True
	except ValueError:
		return False

infilename = sys.argv[1]
outfilename = sys.argv[2]

#read the existing file
fi = open(infilename)
lines = fi.readlines()
fi.close()
splitLines = [x.split('\t') for x in lines]

#fill in the data when the gap is less than 36 hours using linear interpolation
ibaro = 7
i = 0
while i < len(splitLines) - 1:
	i += 1
	if not isNumber(splitLines[i][ibaro]):
		lastGood = float(splitLines[i-1][ibaro])
		numBad = 1
		while not isNumber(splitLines[i + numBad][ibaro]):
			numBad += 1
		nextGood = float(splitLines[i + numBad][ibaro])
		if numBad < 144:#the data are 15 minutes apart, so 144 in a row means 36 hours
			for k in range(0, numBad):
				splitLines[i + k][ibaro] = str(lastGood * (numBad - k) / (numBad + 1) + nextGood * (k + 1) / (numBad + 1)) + "\n"
				print [lastGood, splitLines[i + k][ibaro], nextGood, numBad]
		i += numBad - 1

#fill in the data when the gap is less than 36 hours using linear interpolation
itemp = 6
i = 0
while i < len(splitLines) - 1:
	i += 1
	if not isNumber(splitLines[i][itemp]):
		lastGood = float(splitLines[i-1][itemp])
		numBad = 1
		while not isNumber(splitLines[i + numBad][itemp]):
			numBad += 1
		nextGood = float(splitLines[i + numBad][itemp])
		if numBad < 144:#the data are 15 minutes apart, so 144 in a row means 36 hours
			for k in range(0, numBad):
				splitLines[i + k][itemp] = str(lastGood * (numBad - k) / (numBad + 1) + nextGood * (k + 1) / (numBad + 1))
				print [lastGood, splitLines[i + k][itemp], nextGood, numBad]
		i += numBad - 1


""" This code was abandoned, and 
#fill in the bigger gaps using a more advanced interpolation scheme
for i in range(len(splitLines)):
	if not isNumber(splitLines[i][ibaro]):
		numBad = 1
		while not isNumber(splitLines[i + numBad][ibaro]):
			numBad += 1
			"""
		

#make sure certain columns have a leading zero
jdoubledigits = [0, 1, 3, 4, 5]
for i in range(0, len(splitLines)):
	for j in range(0, len(jdoubledigits)):
		x = int(splitLines[i][j])
		if x < 10:
			splitLines[i][j] = "0" + str(x)

#write the file
fo = open(outfilename, "w")
for i in range(0, len(splitLines)):
	fo.write('\t'.join(splitLines[i]))
fo.close()

