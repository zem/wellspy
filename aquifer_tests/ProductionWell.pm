#!/usr/bin/perl
use DBI;
package ProductionWell;

sub new
{
	my $class = shift;
	my $self = { };
	bless $self, $class;
	return $self;
}

sub setName
{
	my( $self, $name ) = @_;
	$self->{_name} = $name if defined( $name );
	return $self->{_name};
}

sub getName
{
	my( $self ) = @_;
	return $self->{_name};
}

sub setBaseName
{
	my( $self, $basename ) = @_;
	$self->{_baseName} = $basename if defined( $basename );
	return $self->{_baseName};
}

sub getBaseName
{
	my( $self ) = @_;
	return $self->{_baseName};
}

sub setPlot
{
	my( $self, $boolplot ) = @_;
	if( $boolplot == 2 )
	{
		$self-> {_plot} = 1;
	}
	return $self->{_plot};
}

sub getPlot
{
	my( $self ) = @_;
	return $self->{_plot};
}

sub setBeginTime
{
	my( $self, $beginTime ) = @_;
	$self->{_beginTime} =  $beginTime if defined( $beginTime );
	return $self->{_beginTime};
}

sub getBeginTime
{
	my( $self ) = @_;
	return $self->{_beginTime};
}

sub setEndTime
{
	my( $self, $endTime ) = @_;
	$self->{_endTime} =  $endTime if defined( $endTime );
	return $self->{_endTime};
}

sub getEndTime
{
	my( $self ) = @_;
	return $self->{_endTime};
}

sub setdbHandle
{
	my( $self, $dbHandle ) = @_;
	$self->{_dbHandle} =  $dbHandle if defined( $dbHandle );
	return $self->{_endTime};
}

sub querysetPumpRate
{
	my( $self ) = @_;
	my $sth;
	my $prate;
	my $nrows;
# Prepare and execute query for pumping rate
	$sth = $self->{_dbHandle}->prepare( "SELECT pumprategpm from PumpRate20080111 WHERE wellname REGEXP \'^$self->{_name}\$\'" );
	$sth->execute() or die "SQL Error: $DBI::errstr\n";
	if( $sth->rows > 1 )
	{
		$nrows = $sth->rows;
		die print "\nNon-unique query for pumping rate for $self->{_name}, $nrows records found!\n";
	}
	$prate = $sth->fetchrow_array();
	$prate = $prate * 60 * 24 / 264.172;  # Convert from gpm to m3/day
	if( $sth->rows == 0 )
	{
		die print "No match for production rate for $self->{_name}!\n";
	}
	$sth = ();
	$self->{_pumpRate} = $prate;
	return $self->{_pumpRate};
}

sub getPumpRate
{
	my( $self ) = @_;
	return $self->{_pumpRate};
}

sub querysetCoordinates
{
	my( $self ) = @_;
	my $sth;
	my @xcoords;
	my $xcoord;
	my $ycoord;
	my $nrows;
# Prepare and execute query for well coordinates
	$sth = $self->{_dbHandle}->prepare( "SELECT X_COORD,Y_COORD from WQDBLocation WHERE LOCATION_NAME REGEXP \'^$self->{_name}\$\'" );
	$sth->execute() or die "SQL Error: $DBI::errstr\n";
	if( $sth->rows > 1 )
	{
		$nrows = $sth->rows;
		die print "\nNon-unique query for coordinates for $self->{_name}, $nrows records found!\n";
	}
	@coords = $sth->fetchrow_array();
	$self->{_xcoord} = $coords[0] * 0.3048;   # Convert feet to meters
	$self->{_ycoord} = $coords[1] * 0.3048;
	if( $sth->rows == 0 )
	{
		die print "No matches for coordinates for $self->{_name}!\n";
	}
	$sth = ();
}

sub getCoordinates
{
	my( $self ) = @_;
	return ( $self->{_xcoord}, $self->{_ycoord} );
}

sub getX
{
	my( $self ) = @_;
	return $self->{_xcoord};
}

sub getY
{
	my( $self ) = @_;
	return $self->{_ycoord};
}

sub querysetRadius
{
	my( $self ) = @_;
	my $sth;
# Prepare and execute query for well radius
	$sth = $self->{_dbHandle}->prepare( "SELECT INNER_DIAM from ScreenData20080310 WHERE WELL_NAME REGEXP \'^$self->{_name}\$\'" );
	$sth->execute() or die "SQL Error: $DBI::errstr\n";
	if( $sth->rows > 1 )
	{
		die print "\nNon-unique query for well radius for $self->{_name}!\n";
	}
	@radiusin = $sth->fetchrow_array();
	$self->{_radius} = $radiusin[0] * 0.0254;   # Convert inches to meters
	if( $sth->rows == 0 )
	{
		die print "No match for well radius for $self->{_name}!\n";
	}
	$sth = ();
}

sub getRadius
{
	my( $self ) = @_;
	return $self->{_radius};
}

sub querysetRates
{
	my( $self ) = @_;
	my $nrows;
	my @srate = ();
	my @sttime = ();
	my $count = 0;
	#Create production  and time arrays for plotting
	if( $self->{_plot} )
	{
		my @production = ();
		my @times = ();
		my @unixtime = ();
	}
	print "Pumping well $self->{_name} ";
# Prepare and execute query for volume pumped each day
	$sth = $self->{_dbHandle}->prepare( "SELECT datediff(date(time),\'1899-12-30\'),productiongal,date(time) from ProdDaily WHERE wellname REGEXP \"^$self->{_name}\$\" and time > $self->{_beginTime} and time < $self->{_endTime}" );
	$sth->execute() or die "SQL Error: $DBI::errstr\n";
	$nrows = $sth->rows;
	if( $nrows > 0 )
	{
		my @stdate = ();
		my $wellno = $i + 1;
		my $prev_zero = 0;
		my $time = 0;
		my @prodvol = ();

		for( $j = 0; $j < $nrows; $j++ )
		{
			@prodvol = $sth->fetchrow_array();
			$volgal = $prodvol[1] / 264.172;  # Convert gal to m^3
			if( $self-> {_plot} )
			{
				$production[$j] = $prodvol[1];
				$times[$j] = $prodvol[2];
				$unixtime[$j] = $prodvol[0];
			}
			$time = $volgal / $self-> {_pumpRate}; # Start at 4th column of infl
			if( $time > 0.95 || ( $time == 0 && $prev_zero == 0 ) )
			{
				$srate[$count] = $volgal;    #Pumping rate equal to the total volume produced
				$sttime[$count] = $prodvol[0]; # Use day since 1900 for time
				$stdate[$count] = $prodvol[2];   # Used for screen output of begin and end time
				$stdate[$count] =~ s/-//g;
									  $count = $count + 1;
				if( $time == 0 )
				{
					$prev_zero = 1;
				}
				else
				{
					$prev_zero = 0;
				}
			} elsif( $time != 0 )
			{
				$srate[$count] = $self-> {_pumpRate};
				$sttime[$count] = $prodvol[0];
				$stdate[$count] = $prodvol[2];   # Used for screen output of begin and end time
				$stdate[$count] =~ s/-//g;
									  $count = $count + 1;
				$srate[$count] = 0;
				$sttime[$count] = $prodvol[0] + $time;
				$stdate[$count] = $prodvol[2];   # Used for screen output of begin and end time
				$stdate[$count] =~ s/-//g;
									  $count = $count + 1;
				$prev_zero = 1;
			}
			if( $j == $nrows - 1 && $time == 0 && $prev_zero == 1 )
			{
				$srate[$count] = 0;
				$sttime[$count] = $prodvol[0];
				$stdate[$count] = $prodvol[2];   # Used for screen output of begin and end time
				$stdate[$count] =~ s/-//g;
									  $count = $count + 1;
			}
		}
		$sth = ();
		if( $self->{_plot} ) { $self->createPlot( \@production, \@times, \@unixtime, $nrows ); }
		print "Begin $stdate[0] End $stdate[$count-1] Number of records $count\n";
		$self->{_time} = \@sttime; # Create reference to array
		$self->{_rate} = \@srate;  # Create reference to array
		$self->{_count} = $#srate + 1;
		return ( $self->{_time}, $self->{_rate} );
	}
	else
	{
		my @queryresults = ();
		$sth = $self->{_dbHandle}->prepare( "SELECT datediff(date(starttime),\'1899-12-30\'),datediff(date(endtime),\'1899-12-30\'),pumprategpm,date(starttime),date(endtime) from PumpTest WHERE wellname REGEXP \"^$self->{_name}\$\" and starttime > $self->{_beginTime} and endtime < $self->{_endTime} order by endtime" );
		$sth->execute() or die "SQL Error: $DBI::errstr\n";
		$nrows = $sth->rows;
		if( $nrows == 0 )
		{
			die "Neither daily rate nor pump test data for $self->{_name}\n"
		}
		for( $j = 0; $j < $nrows; $j++ )
		{
			@row = $sth->fetchrow_array();
			$srate[$count] = $row[2] * 60 * 24 / 264.172;
			$sttime[$count] = $row[0];
			$stdate[$count] = $row[3];
			$stdate[$count] =~ s/-//g;
			$count = $count + 1;
			$srate[$count] = 0;
			$sttime[$count] = $row[1];
			$stdate[$count] = $row[4];
			$stdate[$count] =~ s/-//g;
			$count = $count + 1;
		}
		$sth = ();
		print "Begin $stdate[0] End $stdate[$count-1] Number of records $count\n";
		$self->{_time} = \@sttime; # Create reference to array
		$self->{_rate} = \@srate;  # Create reference to array
		$self->{_count} = $#srate + 1;
		return ( $self->{_time}, $self->{_rate} );
	}
}

sub getCount
{
	my( $self ) = @_;
	return $self->{_count};
}

sub getRate
{
	my( $self, $index ) = @_;
	my $rate = $self->{_rate}; # Create reference to array
	return $$rate[$index]; # Dereference array to get value
}

sub getTime
{
	my( $self, $index ) = @_;
	my $time = $self->{_time}; # Create reference to array
	return $$time[$index]; # Dereference array to get value
}

sub createPlot
{
	my( $self ) = shift;
	my( $production ) = shift;
	my( $time ) = shift;
	my( $unixtime ) = shift;
	my( $nrows ) = shift;
	my @month = ();
	print $nrows . "\n";
# Open PLT file for plotting pumping well production
	my( $pltflnm ) = $self->{_baseName} . "-$self->{_name}.dat";
	open( pltfl, ">$pltflnm" ) || die "Can't open PLT file: $pltflnm\n";

	for( my $i = 0; $i < $nrows; $i++ )
	{
		my $o = $unixtime[$i] + 2415019;
		my @segs = split( /-/ , $$time[$i] );
		$month[$i] = $segs[1];
		print pltfl "$$unixtime[$i] $o $$production[$i] \n";
	}
	close( pltfl );
	my @args = ( "./prod-plotm", "$self->{_baseName}-$self->{_name}.dat", "$self->{_name}", "impulses" );
	system( @args ) == 0 || die "system @args failed for $self->{_name}!!!\n";

##################################################
# Postprocess for monthly averaged head measurements
##################################################

	my( $prev_month ) = $month[0];
	my( $prev_time ) = $$unixtime[0];
	my( $sum_vols ) = 0;
	$k = 0;
	my( $num ) = 0;
	my( $month_count ) = 0;
	@month_vols = ();
	@month_obstime = ();
	for( $k = 0; $k < $nrows; $k++ )
	{
		if( $prev_month == $month[$k] )
		{
			$sum_vols += $$production[$k];
			$num++;
		}
		else
		{
			if( $num > 0 )
			{
				$month_vols[$month_count] = $sum_vols / $num;
			}
			else
			{
				$month_vols[$month_count] = $$heads[$k];
				$prev_month = $month[$k];
				$prev_time = $$unixtime[$k];
			}
			$month_obstime[$month_count] = $prev_time;
			$sum_vols = 0;
			$num = 0;
			$month_count++;
		}
		$prev_month = $month[$k];
		$prev_time = $$unixtime[$k];
	}

	$pltflnm = "$self->{_baseName}-$self->{_name}-MONTH.dat";
	open( pltfl, ">$pltflnm" ) || die "Can't open PLT file: $pltflnm\n";

	for( $k = 0; $k < $month_count; $k++ )
	{
		$o = $month_obstime[$k] + 2415019;
		print pltfl "$month_obstime[$k] $o $month_vols[$k]\n";
	}
	close( pltfl );
	my @args = ( "./prod-plotm", "$self->{_baseName}-$self->{_name}-MONTH.dat", "$self->{_name}", "impulses" );
	system( @args ) == 0 || die "system @args failed for $self->{_name}!!!\n";
#unlink("$ARGV[0]-$wells[$i].dat");


}


1;
