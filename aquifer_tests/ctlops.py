#import os
import subprocess
import multiprocessing

def runDecompWellsSupport(rootfilename):
	subprocess.call("python decomp-wells " + rootfilename, shell=True)

def runGetDrawdownSupport(rootfilename):
	subprocess.call("python get-drawdown " + rootfilename, shell=True)

def runCreateWellsSupport(zipped):
	ctlfilename = zipped[0]
	rootfilename = zipped[1]
	subprocess.call("./create-wells " + ctlfilename + " " + rootfilename, shell=True)

def runMadsSupport(rootfilename):
	#subprocess.call("mads " + rootfilename + ".pst quiet igrnd real=100", shell=True)
	subprocess.call("mads " + rootfilename + ".pst ldebug", shell=True)

numProcs = 19
pool = multiprocessing.Pool(numProcs)

def getctlFilenames(directory):
	proc = subprocess.Popen("ls -1 " + directory + "/*.ctl", stdout=subprocess.PIPE, shell=True)
	lines = proc.stdout.readlines()
	filenames = map(lambda x: x.rstrip(), lines)
	return filenames

def changeLine(newline, linenum, infilename, outfilename=[]):
	if outfilename == []:
		outfilename = infilename
	with open(infilename) as f:
		lines = f.readlines()
		f.close()
	lines[linenum] = newline + "\n"
	with open(outfilename, "w") as f:
		for line in lines:
			f.write(line)
		f.close()

def setProductionWells(infilename, wells, outfilename=[]):
	line = "Production Wells:" + wells
	changeLine(line, 1, infilename, outfilename)

def setBeginTime(infilename, time, waterLevelTime=[], outfilename=[]):
	if waterLevelTime == []:
		line = "Begin time :" + time
	else:
		line = "Begin time :" + time + " " + waterLevelTime
	changeLine(line, 5, infilename, outfilename)

def setEndTime(infilename, time, outfilename=[]):
	line = "End time :" + time
	changeLine(line, 6, infilename, outfilename)

def mungeFiles(dir1, dir2, prodWells="PM-02 PM-03 PM-04 PM-05 R-28 R-42", beginTime="20111230", waterLevelTime="20121230", endTime="20131230"):
	infilenames = getctlFilenames(dir1)
	infilenamesNoDir = map(lambda x: x.split("/")[-1], infilenames)
	outfilenames = map(lambda x: dir2 + "/" + x, infilenamesNoDir)
	for i in range(0, len(infilenames)):
		if "r36" in infilenames[i]:
			if "R-28" in prodWells:
				if "R-42" in prodWells:
					setProductionWells(infilenames[i], "O-04 PM-01 PM-02 PM-03 PM-04 R-28 R-42", outfilename=outfilenames[i])
				else:
					setProductionWells(infilenames[i], "O-04 PM-01 PM-02 PM-03 PM-04 R-28", outfilename=outfilenames[i])
			else:
				if "R-42" in prodWells:
					setProductionWells(infilenames[i], "O-04 PM-01 PM-02 PM-03 PM-04 R-42", outfilename=outfilenames[i])
				else:
					setProductionWells(infilenames[i], "O-04 PM-01 PM-02 PM-03 PM-04", outfilename=outfilenames[i])
		else:
			setProductionWells(infilenames[i], prodWells, outfilename=outfilenames[i])
		setBeginTime(outfilenames[i], beginTime, waterLevelTime=waterLevelTime)
		setEndTime(outfilenames[i], endTime)
	return infilenamesNoDir

def createManyTests(dir1, dir2):
	#names = ["1R-281R-42", "1R-280R-42", "0R-280R-42"]
	#prodWells = ["O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42", "O-04 PM-02 PM-03 PM-04 PM-05 R-28", "O-04 PM-02 PM-03 PM-04 PM-05"]
	#beginTimes = ["20111230", "20101230", "20091230", "20081230", "20071230"]
	#waterLevelTimes = ["20121230", "20111230", "20101230", "20091230", "20081230", "20071230"]
	#beginTimes = ["20091230", "20100930", "20100530"]
	#waterLevelTimes = ["20111230", "20120930", "20120530"]
	#prodWells = ["O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42"]
	#beginTimes = ["20071230"]
	#waterLevelTimes = ["20071230"]
	names = ["few", "all"]
	prodWells = ["PM-04 R-28 R-42", "O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42"]
	beginTimes = ["20100930"]
	waterLevelTimes = ["20120930"]
	for i in range(len(prodWells)):
		for j in range(len(beginTimes)):
			newdir = dir2 + "/" + beginTimes[j]
			if i == 0:
				subprocess.call("mkdir " + newdir, shell=True)
			newdir = dir2 + "/" + beginTimes[j] + "/" + names[i]
			subprocess.call("mkdir " + newdir, shell=True)
			mungeFiles(dir1, newdir, prodWells=prodWells[i], beginTime=beginTimes[j], waterLevelTime=waterLevelTimes[j])
			subprocess.call("cp " + dir1 + "/*.pst " + newdir, shell=True)
			subprocess.call("cp " + dir1 + "/*.tpl " + newdir, shell=True)
			subprocess.call("cp " + dir1 + "/*.ins " + newdir, shell=True)
			runCreateWells(newdir)

def runManyTests(dir1, dir2):
	names = ["longtime"]
	prodWells = ["O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42"]
	beginTimes = ["20101230"]
	waterLevelTimes = ["20110630"]
	#names = ["1R-281R-42", "1R-280R-42", "0R-280R-42"]
	#prodWells = ["O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42", "O-04 PM-02 PM-03 PM-04 PM-05 R-28", "O-04 PM-02 PM-03 PM-04 PM-05"]
	#beginTimes = ["20111230", "20101230", "20091230", "20081230", "20071230"]
	#waterLevelTimes = ["20121230", "20111230", "20101230", "20091230", "20081230", "20071230"]
	#beginTimes = ["20091230", "20100930", "20100530"]
	#waterLevelTimes = ["20111230", "20120930", "20120530"]
	#prodWells = ["O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42"]
	#beginTimes = ["20071230"]
	#waterLevelTimes = ["20071230"]
	#names = ["few"]
	#prodWells = ["PM-02 PM-04 R-28 R-42"]
	#beginTimes = ["20100930"]
	#waterLevelTimes = ["20120930"]
	for i in range(len(prodWells)):
		for j in range(len(beginTimes)):
			newdir = dir2 + "/" + beginTimes[j]
			if i == 0:
				subprocess.call("mkdir " + newdir, shell=True)
			newdir = dir2 + "/" + beginTimes[j] + "/" + names[i]
			subprocess.call("mkdir " + newdir, shell=True)
			mungeFiles(dir1, newdir, prodWells=prodWells[i], beginTime=beginTimes[j], waterLevelTime=waterLevelTimes[j])
			subprocess.call("cp " + dir1 + "/*.pst " + newdir, shell=True)
			subprocess.call("cp " + dir1 + "/*.tpl " + newdir, shell=True)
			subprocess.call("cp " + dir1 + "/*.ins " + newdir, shell=True)
			runCreateWells(newdir)
	for i in range(len(prodWells)):
		for j in range(len(beginTimes)):
			newdir = dir2 + "/" + beginTimes[j] + "/" + names[i]
			runMads(newdir)
	for i in range(len(prodWells)):
		for j in range(len(beginTimes)):
			newdir = dir2 + "/" + beginTimes[j] + "/" + names[i]
			runDecompWells(newdir)

def getManyDrawdowns(dir2):
	names = ["1R-281R-42", "1R-280R-42", "0R-280R-42"]
	prodWells = ["O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42", "O-04 PM-02 PM-03 PM-04 PM-05 R-28", "O-04 PM-02 PM-03 PM-04 PM-05"]
	#beginTimes = ["20111230", "20101230", "20091230", "20081230", "20071230"]
	#waterLevelTimes = ["20121230", "20111230", "20101230", "20091230", "20081230", "20071230"]
	beginTimes = ["20091230", "20100930", "20100530"]
	waterLevelTimes = ["20111230", "20120930", "20120530"]
	#prodWells = ["O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42"]
	#beginTimes = ["20071230"]
	#waterLevelTimes = ["20071230"]
	#names = ["few"]
	#prodWells = ["PM-02 PM-04 R-28 R-42"]
	#beginTimes = ["20100930"]
	#waterLevelTimes = ["20120930"]
	for i in range(len(prodWells)):
		for j in range(len(beginTimes)):
			newdir = dir2 + "/" + beginTimes[j] + "/" + names[i]
			runGetDrawdown(newdir)

def runManyTestsNoCreate(dir2):
	#names = ["1R-281R-42", "1R-280R-42", "0R-280R-42"]
	#prodWells = ["O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42", "O-04 PM-02 PM-03 PM-04 PM-05 R-28", "O-04 PM-02 PM-03 PM-04 PM-05"]
	#beginTimes = ["20111230", "20101230", "20091230", "20081230", "20071230"]
	#waterLevelTimes = ["20121230", "20111230", "20101230", "20091230", "20081230", "20071230"]
	#beginTimes = ["20091230", "20100930", "20100530"]
	#waterLevelTimes = ["20111230", "20120930", "20120530"]
	#prodWells = ["O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42"]
	#beginTimes = ["20071230"]
	#waterLevelTimes = ["20071230"]
	names = ["few", "all"]
	prodWells = ["PM-04 R-28 R-42", "O-04 PM-02 PM-03 PM-04 PM-05 R-28 R-42"]
	beginTimes = ["20100930"]
	waterLevelTimes = ["20120930"]
	for i in range(len(prodWells)):
		for j in range(len(beginTimes)):
			newdir = dir2 + "/" + beginTimes[j] + "/" + names[i]
			runMads(newdir)
	for i in range(len(prodWells)):
		for j in range(len(beginTimes)):
			newdir = dir2 + "/" + beginTimes[j] + "/" + names[i]
			runDecompWells(newdir)

def runCreateWells(dir):
	ctlfilenames = getctlFilenames(dir)
	rootfilenames = map(lambda x: x.split(".")[0], ctlfilenames)
	zip_list = zip(ctlfilenames, rootfilenames)
	pool.map(runCreateWellsSupport, zip_list)

def runMads(dir):
	ctlfilenames = getctlFilenames(dir)
	rootfilenames = map(lambda x: x.split(".")[0], ctlfilenames)
	pool.map(runMadsSupport, rootfilenames)

def runDecompWells(dir):
	ctlfilenames = getctlFilenames(dir)
	rootfilenames = map(lambda x: x.split(".")[0], ctlfilenames)
	pool.map(runDecompWellsSupport, rootfilenames)

def runGetDrawdown(dir):
	ctlfilenames = getctlFilenames(dir)
	rootfilenames = map(lambda x: x.split(".")[0], ctlfilenames)
	pool.map(runGetDrawdownSupport, rootfilenames)

def getParamValues(rootname, params, paramTransform=lambda x: x):
	f = open(rootname + ".results")
	lines = f.readlines()
	f.close()
	paramValues = {}
	for line in lines:
		splitLine = line.split(' ')
		if len(splitLine) > 0 and splitLine[0] in params:
			paramValues[splitLine[0]] = paramTransform(float(splitLine[1].strip()))
	return paramValues

def getBestParamValues():
	bestRootnames = ["newdb/t4/20091230/1R-281R-42/r11-v01", "newdb/t4/20100930/1R-281R-42/r13-v01", "newdb/t4/20091230/1R-281R-42/r15-v01", "newdb/t4/20100530/1R-281R-42/r28-v01", "newdb/t4/20091230/1R-281R-42/r35b-v01", "newdb/t4/20091230/1R-280R-42/r36-v01", "newdb/r42all/r42-v04", "newdb/t4/20091230/1R-281R-42/r43#1-v01", "newdb/t4/20091230/1R-281R-42/r43#2-v01", "newdb/t4/20100930/1R-280R-42/r44#1-v01", "newdb/t4/20091230/1R-280R-42/r44#2-v01", "newdb/t4/20100930/1R-280R-42/r45#1-v01", "newdb/t4/20100930/1R-280R-42/r45#2-v01", "newdb/t4/20091230/1R-281R-42/r50#1-v01", "newdb/t4/20091230/1R-281R-42/r50#2-v01", "newdb/t4/20100930/1R-281R-42/r61#1-v02", "newdb/t4/20100930/1R-281R-42/r61#2-v02", "newdb/t4/20100930/1R-281R-42/r62-v01", "newdb/t4/20100530/1R-281R-42/r62-v01"]
	for rootname in bestRootnames:
		print rootname
		print getParamValues(rootname, ["K_R-42", "S_R-42", "K_R-28", "S_R-28"], paramTransform=lambda x: 10**x)

#mungeFiles("setup", "test2014")
#runCreateWells("test2014")
#runMads("test2014")
#runManyTests("setup", "newdb/t4")
#getManyDrawdowns("newdb/t4")
#runManyTestsNoCreate("ma/fixedr612")
#runDecompWells("ma/fixedr612/20100930/few")
#runManyTestsNoCreate("ma/igrnd100")
#runGetDrawdown("ma/t8/20091230/1R-281R-42")
#runGetDrawdown("ma/t8/20100530/1R-281R-42")
#runGetDrawdown("ma/t8/20100930/1R-281R-42")
#runCreateWells("ma/r61/fewwells")
#runMads("ma/r61/fewwells")
#runMads("newdb/t4/20100530/0R-280R-42/")
#runDecompWells("newdb/t4/20100530/0R-280R-42/")
#runMads("newdb/t4/20091230/0R-280R-42/")
#runDecompWells("newdb/t4/20091230/0R-280R-42/")
#runManyTests("setup", "newestdb")
#runManyTests("setup", "newesterdb")
runDecompWells("newesterdb/20101230/longtime")
#getBestParamValues()
