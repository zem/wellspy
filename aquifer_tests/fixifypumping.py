#The purpose of this script is to replace the 1s in the first column
#of the pumping_data file with increasing values starting at the
#third command line argument.
#To find out where to start, "run select max(id) from ProdDaily;" at
#the mysql command prompt and then start at one higher than that.
import sys

infilename = sys.argv[1]
outfilename = sys.argv[2]
startingNumber = int(sys.argv[3])

fi = open(infilename)
lines = fi.readlines()
fi.close()
splitLines = [x.split(',') for x in lines]

for i in range(0, len(splitLines)):
	splitLines[i][0] = str(startingNumber + i)

fo = open(outfilename, "w")
for i in range(0, len(splitLines)):
	fo.write(','.join(splitLines[i]))
fo.close()
