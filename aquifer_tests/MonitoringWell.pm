#!/usr/bin/perl
use DBI;
package MonitoringWell;

sub new
{
	my $class = shift;
	my $self = { };
	bless $self, $class;
	return $self;
}

sub setName
{
	my( $self, $name ) = @_;
	$self->{_name} = $name if defined( $name );
	return $self->{_name};
}

sub getName
{
	my( $self ) = @_;
	return $self->{_name};
}

sub setBaseName
{
	my( $self, $basename ) = @_;
	$self->{_baseName} = $basename if defined( $basename );
	return $self->{_baseName};
}

sub getBaseName
{
	my( $self ) = @_;
	return $self->{_baseName};
}

sub setPlot
{
	my( $self, $boolplot ) = @_;
	if( $boolplot > 0 )
	{
		$self->{_plot} = 1;
	}
	return $self->{_plot};
}

sub getPlot
{
	my( $self ) = @_;
	return $self->{_plot};
}

sub setBeginTime
{
	my( $self, $beginTime ) = @_;
	$self->{_beginTime} =  $beginTime if defined( $beginTime );
	return $self->{_beginTime};
}

sub getBeginTime
{
	my( $self ) = @_;
	return $self->{_beginTime};
}

sub setEndTime
{
	my( $self, $endTime ) = @_;
	$self->{_endTime} =  $endTime if defined( $endTime );
	return $self->{_endTime};
}

sub getEndTime
{
	my( $self ) = @_;
	return $self->{_endTime};
}

sub setPortDescription
{
	my( $self, $portDescription ) = @_;
	$self->{_portDescription} =  $portDescription if defined( $portDescription );
	return $self->{_portDescription};
}

sub getPortDescription
{
	my( $self ) = @_;
	return $self->{_portDescription};
}

sub setBaroEff
{
	my( $self, $baroEff ) = @_;
	$self->{_baroEff} =  $baroEff if defined( $baroEff );
	return $self->{_baroEff};
}

sub getBaroEff
{
	my( $self ) = @_;
	return $self->{_baroEff};
}

sub setInitialHead
{
	my( $self, $initialHead ) = @_;
	$self->{_initialHead} =  $initialHead if defined( $initialHead );
	return $self->{_initialHead};
}

sub getInitialHead
{
	my( $self ) = @_;
	return $self->{_initialHead};
}

sub setGroupName
{
	my( $self, $groupName ) = @_;
	$self->{_groupName} =  $groupName if defined( $groupName );
	return $self->{_groupName};
}

sub getGroupName
{
	my( $self ) = @_;
	return $self->{_groupName};
}

sub setdbHandle
{
	my( $self, $dbHandle ) = @_;
	$self->{_dbHandle} =  $dbHandle if defined( $dbHandle );
	return $self->{_endTime};
}

sub querysetCoordinates
{
	my( $self ) = @_;
	my $sth;
	my @xcoords;
	my $xcoord;
	my $ycoord;
	my $nrows;
# Prepare and execute query for well coordinates
	$sth = $self->{_dbHandle}->prepare( "SELECT X_COORD,Y_COORD from WQDBLocation WHERE LOCATION_NAME REGEXP \'^$self->{_name}\$\'" );
	$sth->execute() or die "SQL Error: $DBI::errstr\n";
	if( $sth->rows > 1 )
	{
		$nrows = $sth->rows;
		die print "\nNon-unique query for coordinates for $self->{_name}, $nrows records found!\n";
	}
	@coords = $sth->fetchrow_array();
	$self->{_xcoord} = $coords[0] * 0.3048;   # Convert feet to meters
	$self->{_ycoord} = $coords[1] * 0.3048;
	if( $sth->rows == 0 )
	{
		die print "No matches for coordinates for $self->{_name}!\n";
	}
	$sth = ();
	return ( $self->{_xcoord}, $self->{_ycoord} );
}

sub getCoordinates
{
	my( $self ) = @_;
	return ( $self->{_xcoord}, $self->{_ycoord} );
}

sub getX
{
	my( $self ) = @_;
	return $self->{_xcoord};
}

sub getY
{
	my( $self ) = @_;
	return $self->{_ycoord};
}

sub querysetElevation
{
	my( $self ) = @_;
	my $sth;
# Collect well elevation
	$sth = $self->{_dbHandle}->prepare( "SELECT SURFACE_ELEVATION,ELEV_UOM FROM WQDBLocation WHERE LOCATION_NAME REGEXP \'^$self->{_name}\$\'" );
	$sth->execute() or die "SQL Error: $DBI::errstr\n";
	if( $sth->rows > 1 )
	{
		die print "\nNon-unique query for surface elevation for $self->{_name}!\n";
	}
	my @elevwell = $sth->fetchrow_array();
	if( $elevwell[1] ne ft )
	{
		die "Surface elevation units $elevwell[1] not feet for $self->{_name}\n";
	}
	if( $sth->rows == 0 )
	{
		print "No well elevation match for $self->{_name}\n";
	}
	$self->{_elevation} = $elevwell[0];
	$sth = ();
	return $self->{_elevation};
}

sub getElevation
{
	my( $self ) = @_;
	return $self->{_elevation};
}

sub querysetHeads
{
	my( $self ) = @_;
	print "Monitoring Well $self->{_name} Port $self->{_portDescription}\n";
	my $sth;
#
# Prepare and execute query for ALL head measurements
#
	if( $self->{_portDescription} eq '0' )
	{
		$sth = $self->{_dbHandle}->prepare( "SELECT datediff(time,\'1899-12-30\'),time,replace(format(piezometricWLft,9),',',''),temperatureC,time(time),date(time) FROM GroundWaterLevelData WHERE probeno < 10 AND wellname REGEXP \'^$self->{_name}\$\' AND dataqualcode REGEXP \'^\$|V|VR|VQ|VRVQ\' and time > $self->{_beginTime} AND time < $self->{_endTime} GROUP BY time" );
	} else {
		$sth = $self->{_dbHandle}->prepare( "SELECT datediff(time,\'1899-12-30\'),time,replace(format(piezometricWLft,9),',',''),temperatureC,time(time),date(time) FROM GroundWaterLevelData WHERE portdesc REGEXP \'$self->{_portDescription}\' AND probeno < 10 AND wellname REGEXP \'^$self->{_name}\$\' AND dataqualcode REGEXP \'^\$|V|VR|VQ|VRVQ\' and time > $self->{_beginTime} AND time < $self->{_endTime} GROUP BY time" );
	}
	$sth->execute() or die "SQL Error: $DBI::errstr\n";
	my $c = $sth->rows;
	if( $sth->rows == 0 )
	{
		die print "No observations for observation well $self->{_name} at port $self->{_portDescription}!\n";
	}
	else {
		print "Number of all observations $c\n";
	}
#if($self->{_plot}) {
#	my $name;
#	if ($self->{_portDescription} eq '0') { $name = $self->{_name}; }
#	else { $name = "$self->{_name}_$self->{_portDescription}"; }
#	#$name =~ s/-//;
#	$pltflnm = "$self->{_baseName}-$name-ALL.dat";
#	open(pltfl, ">$pltflnm") || die "Can't open PLT file: $pltflnm\n";
#}
# Collect entries from query
	$self->{_day} = ();
	my @date = ();
	my @month = ();
	$self->{_heads} = ();
	$self->{_uncheads} = ();
	$self->{_time} = ();
	my( $diffday ) = 0;
	my( $count ) = 0;
	my( $no_baro_cnt ) = 0;
	my $baroflnm = "$self->{_baseName}-$name.baro";
	open( barfl, ">$baroflnm" ) || die "Can't open BARO file: $baroflnm!\n";
	while( @entry = $sth->fetchrow_array() )
	{
		# Convert time to decimal days and add to days since 1900
		my( @time ) = split( /:/ , $entry[4] ); # time
		my( $dectime ) = ( ( $time[2] / 60 + $time[1] ) / 60 + $time[0] ) / 24;
		$self->{_time} [$count] = $entry[0] + $dectime;
		# Collect days since 1900
		$day[$count] = $entry[0];
		$self->{_day} [$count] = $entry[0];
		$date[$count] = $entry[5]; #Use for screen output
		$date[$count] =~ s/-//g;
		@segs = split( /-/ , $entry[5] );
		$month[$count] = $segs[1];
		if( $count > 0 ) { $diffday = $day[$count] - $day[$count - 1]; }
		else { $diffday = 0; }
# Convert feet to meters
		$self->{_uncheads} [$count] = $entry[2] * 0.3048; # Save uncorrected heads
# Correct for barometric pressure
		if( $self->{_baroEff} > 0 )
		{
			$self->{_heads} [$count] = &AdjBaroTA54( $self->{_time} [$count], $entry[1], $entry[2], $entry[3], $self->{_elevation}, $self->{_baroEff}, $self->{_dbHandle} ) * 0.3048;
			if( $self->{_heads} [$count] == 0 )
			{
				$no_baro_cnt++;
				next;
			}
# Shift corrected heads according to calibrated reading, likely the first reading after a break in readings
			if( $count == 0 || $diffday > 1.1 )
			{
				$adjcorr = $self->{_uncheads} [$count] - $self->{_heads} [$count];
				$self->{_heads} [$count] = $self->{_uncheads} [$count];
			}
			else
			{
				$self->{_heads} [$count] = $self->{_heads} [$count] + $adjcorr;
			}
		}
		else { $self->{_heads} [$count] = $self->{_uncheads} [$count]; }
#if($diffday>1.1) {print pltfl "\n";}
#$o = $self->{_time}[$count] + 2415019;
#print pltfl "$self->{_time}[$count] $o $self->{_uncheads}[$count] $self->{_heads}[$count]\n";
		$count++;
	}
	print "Number of missing baro data at TA-54 for $self->{_name} record $no_baro_cnt (see $baroflnm for details)\n";
	close( barfl );
	$self->setBeginTime( $date[0] );
	$self->setEndTime( $date[$count - 1] );
#print "Begin $date[0] End $date[$count-1] Number of records $count\n";

	$self->{_count} = $count;
	$sth = ();

# Postprocess for average heads
	$self->avgHeads( \@unixtime );
# Postprocess for first daily heads
	$self->firstHeads( \@unixtime );

	if( $self->{_plot} ) { $self->createPlot( \@production, \@times, \@unixtime, $nrows ); }

	return ( $self->{_time}, $self->{_heads}, $self->{_uncheads} );
}

sub getCount
{
	my( $self ) = @_;
	return $self->{_count};
}

sub getTime
{
	my( $self, $index ) = @_;
	return $self->{_time} [$index];
}

sub getDay
{
	my( $self, $index ) = @_;
	return $self->{_day} [$index];
}

sub getHead
{
	my( $self, $index ) = @_;
	return $self->{_heads} [$index];
}

sub getUncHead
{
	my( $self, $index ) = @_;
	return $self->{_uncheads} [$index];
}

sub getFirstCount
{
	my( $self ) = @_;
	return $self->{_firstcount};
}

sub getFirstHead
{
	my( $self, $index ) = @_;
	return $self->{_firstheads} [$index];
}

sub getFirstUncHead
{
	my( $self, $index ) = @_;
	return $self->{_firstuncheads} [$index];
}

sub getFirstTime
{
	my( $self, $index ) = @_;
	return $self->{_firstobstime} [$index];
}

sub getFirstDay
{
	my( $self, $index ) = @_;
	return $self->{_firstday} [$index];
}

sub avgHeads
{
	my( $self ) = shift;
##################################################
# Postprocess for daily averaged head measurements
##################################################
	my( $prev_time ) = $self->{_time} [0];
#my($prev_time) = $$unixtime[0];
	my( $sum_heads ) = 0;
	my( $sum_uncheads ) = 0;
	$k = 0;
	my( $num ) = 0;
#my($avg_count) = 0;
	$self->{_avgcount} = 0;
	$self->{_avgheads} = ();
#@avg_heads = ();
	@avg_uncheads = ();
	$self->{_avguncheads} = ();
	$self->{_avgobstime} = ();
#@avg_obstime = ();
	for( $k = 0; $k < $self->{_count}; $k++ )
	{
		if( int( $prev_time ) == int( $self->{_time} [$k] ) or $k == $self->{_count} - 1 )
		{
#if(int($prev_time)==int($$unixtime[$k])) {
			$sum_heads += $self->{_heads} [$k];
			$sum_uncheads += $self->{_uncheads} [$k];
			$num++;
		} else
		{
			if( $num > 0 )
			{
				$self->{_avgheads} [$self->{_avgcount}] = $sum_heads / $num;
				$self->{_avguncheads} [$self->{_avgcount}] = $sum_uncheads / $num;
			}
			else
			{
				$self->{_avgheads} [$self->{_avgcount}] = $self->{_heads} [$k];
				$self->{_avguncheads} [$self->{_avgcount}] = $self->{_uncheads} [$k];
#$prev_time = $self->{_time}[$k];
			}
			$self->{_avgobstime} [$self->{_avgcount}] = int( $prev_time );
			$sum_heads = 0;
			$sum_uncheads = 0;
			$num = 0;
			$self->{_avgcount}++;
		}
		$prev_time = $self->{_time} [$k];
	}
# Catch final day
	if( $num > 0 )
	{
		$self->{_avgheads} [$self->{_avgcount}] = $sum_heads / $num;
		$self->{_avguncheads} [$self->{_avgcount}] = $sum_uncheads / $num;
	}
	$self->{_avgobstime} [$self->{_avgcount}] = int( $prev_time );
	$self->{_avgcount}++;

}

sub firstHeads
{
	my( $self ) = shift;
##################################################
# Postprocess for first daily head measurements
##################################################
	my( $prev_time ) = $self->{_time} [0];
	my( $sum_heads ) = 0;
	my( $sum_uncheads ) = 0;
	$k = 0;
	my( $num ) = 0;
	$self->{_firstcount} = 0;
#my($first_count) = 0;
	$self->{_firstheads} = ();
	$self->{_firstuncheads} = ();
#@first_heads = ();
#@first_uncheads = ();
	$self->{_firstobstime} = ();
	$self->{_firstday} = ();
	$self->{_firstdate} = ();
#@first_obstime = ();
	for( $k = 0; $k < $self->{_count}; $k++ )
	{
		if( int( $prev_time ) != int( $self->{_time} [$k] ) or $k == 0 )
		{
			$self->{_firstheads} [$self->{_firstcount}] = $self->{_heads} [$k];
			$self->{_firstuncheads} [$self->{_firstcount}] = $self->{_uncheads} [$k];
#$self->{_firstobstime}[$self->{_firstcount}] = int($prev_time);
			$self->{_firstday} [$self->{_firstcount}] = $day[$k];
			$self->{_firstdate} [$self->{_firstcount}] = $date[$k];
			$self->{_firstobstime} [$self->{_firstcount}] = int( $self->{_time} [$k] );
			$self->{_firstcount}++;
		}
		$prev_time = $self->{_time} [$k];
	}
}

sub createPlot
{
	my( $self ) = shift;
	my( $diffday ) = 0;

	my $name;
	if( $self->{_portDescription} eq '0' ) { $name = $self->{_name}; }
	else { $name = "$self->{_name}_$self->{_portDescription}"; }
#$name =~ s/-//;
	$pltflnm = "$self->{_baseName}-$name-ALL.dat";
	open( pltfl, ">$pltflnm" ) || die "Can't open PLT file: $pltflnm\n";

	for( my $i = 0; $i < $self->{_count}; $i++ )
	{
		if( $i > 0 ) { $diffday = $day[$i] - $day[$i - 1]; }
		if( $diffday > 1.1 ) {print pltfl "\n";}
		$o = $self->{_time} [$i] + 2415019;
		print pltfl "$self->{_time}[$i] $o $self->{_uncheads}[$i] $self->{_heads}[$i]\n";
	}

	close( pltfl );
#$pltflnm = "$self->{_baseName}-$name-ALL.dat";
	@args = ( "./head-plot", "$pltflnm", "$self->{_name}", "lines" );
	system( @args ) == 0 || die "system @args failed!!!\n";

#Plot daily average heads
	$pltflnm = "$self->{_baseName}-$self->{_name}-AVG.dat";
	open( pltfl, ">$pltflnm" ) || die "Can't open PLT file: $pltflnm\n";

	for( $k = 0; $k < $self->{_avgcount}; $k++ )
	{
		$o = $self->{_avgobstime} [$k] + 2415019;
		print pltfl "$self->{_avgobstime}[$k] $o $self->{_avguncheads}[$k] $self->{_avgheads}[$k]\n";
	}
	close( pltfl );
	@args = ( "./head-plot", "$pltflnm", "$self->{_name}", "lines" );
	system( @args ) == 0 || die "system @args failed!!!\n";

# Plot first daily heads
	$pltflnm = "$self->{_baseName}-$self->{_name}-FIRST.dat";
	open( pltfl, ">$pltflnm" ) || die "Can't open PLT file: $pltflnm\n";

	for( $k = 0; $k < $self->{_firstcount}; $k++ )
	{
		$o = $self->{_firstobstime} [$k] + 2415019;
		print pltfl "$self->{_firstobstime}[$k] $o $self->{_firstuncheads}[$k] $self->{_firstheads}[$k]\n";
	}
	close( pltfl );
	@args = ( "./head-plot", "$pltflnm", "$self->{_name}", "lines" );
	system( @args ) == 0 || die "system @args failed!!!\n";

}

sub AdjBaroTA54
{
# Create variable for TA54 elevation
	my( $elevta54 ) = 6548;

# Create variable for average barometric pressure for TA54 (calculated from database)
	my( $avgpress ) = 26.79;

# Assign arguments to variables
	my( $tdec ) = $_[0];
	my( $gwtime ) = $_[1];
	my( $uncorrWL ) = $_[2];
	my( $welltemp ) = $_[3];
	my( $elevwell ) = $_[4];
	my( $baroeff ) = $_[5];
	my( $dbh ) = $_[6];

# Collect barometric pressure at TA-54 at $gwtime
	my( $sth2 ) = $dbh->prepare( "SELECT datediff(time,\'1899-12-30\'),time(time),pressmb,tempC FROM TA54BaroData WHERE time BETWEEN ADDTIME(\'$gwtime\',\'-0 00:15:00\') AND ADDTIME(\'$gwtime\',\'0 00:15:00\')" );
	$sth2->execute() or die "SQL Error: $DBI::errstr\n";
	if( $sth2->rows == 0 ) { print "No barometric pressure data at $gwtime!\n" };

# Collect upper and lower values for interpation
	my( @lower ) = $sth2->fetchrow_array();
	my( @upper ) = $sth2->fetchrow_array();

# Convert time to decimal days and add to days since 1900
	my( @time ) = split( /:/ , $lower[1] );
	my( $dectime ) = ( ( $time[2] / 60 + $time[1] ) / 60 + $time[0] ) / 24;
	my( $ltime ) = $lower[0] + $dectime;

# Convert time to decimal days and add to days since 1900
	my( @time ) = split( /:/ , $upper[1] );
	my( $dectime ) = ( ( $time[2] / 60 + $time[1] ) / 60 + $time[0] ) / 24;
	my( $utime ) = $upper[0] + $dectime;

# Check that @lower and @upper exist
	if( $lower[2] == 0 && $upper[2] != 0 )
	{
		$pressmb = $upper[2];
	} elsif( $upper[2] == 0 && $lower[2] != 0 )
	{
		$pressmb = $lower[2];
	} elsif( $lower[2] == 0 && $upper[2] == 0 )
	{
#                print barfl "Missing barometric data for TA-54 near $gwtime $utime!!!\n";
		return 0;
	} else {
# Perform linear interpolation
		$pressmb = ( $tdec - $ltime ) * ( $upper[2] - $lower[2] ) / ( $utime - $ltime ) + $lower[2];
#                $tempta54 = ($tdec-$ltime)*($upper[3]-$lower[3])/($utime-$ltime)+$lower[3];
	}

# Convert mbar to feet
	my( $pressft ) = $pressmb * 0.0145037 * 2.304;

# Calculate the barometric pressure in the well
	my( $presswell ) = $pressft * exp( -9.80665 / ( 3.281 * 287.04 ) * ( ( $elevwell - $elevta54 ) / ( 275.1 ) + ( $uncorrWL - $elevwell ) / ( $welltemp + 273.15 ) ) );

# Calculate the average barometric pressure in the well (average temp for all wells is used for avg well temp)
	my( $avgpresswell ) = $avgpress * exp( -9.80665 / ( 3.281 * 287.04 ) * ( ( $elevwell - $elevta54 ) / ( 275.1 ) + ( 6590 - $elevwell ) / ( 287.4 ) ) );

# Calculate corrected water level
	my( $presscorr ) = $avgpresswell - $presswell;
	if( $presscorr > 0.2 ) { return 0; }
	my( $corrWL ) = $uncorrWL - $presscorr *$baroeff;

	$sth2 = ();

	return $corrWL;
}

1;
