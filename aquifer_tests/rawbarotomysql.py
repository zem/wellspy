#!/usr/bin/env python

import sys

infilename = sys.argv[1]
outfilename = sys.argv[2]

fi = open(infilename)
lines = fi.readlines()
fi.close()

fo = open(outfilename, "w")
for i in range(len(lines)):
	splitLine = lines[i].split("\t")
	newline = ",".join(["\"" + splitLine[2] + "-" + splitLine[0] + "-" + splitLine[1] + " " + splitLine[3] + ":" + splitLine[4] + ":00\"", splitLine[6], splitLine[7]])
	fo.write(newline)
fo.close()


