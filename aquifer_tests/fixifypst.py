import datetime

#41388
def fixifypst(infilename, outfilename, prejumpday, postfix="_R-42", prefix="P"):
	inf = open(infilename)
	lines = inf.readlines()
	inf.close()
	outf = open(outfilename)
	fixjump = False
	for i in range(len(lines)):
		segs = lines[i].split('\t')
		if fixjump:
			outf.write('\t'.join([segs[0], str(segs[1] + delta), segs[2], segs[3]))
		if not fixjump && segs[0] == prefix + str(prejumpday) + postfix:
			head1 = float(segs[1])
			head2 = float(lines[i + 1].split('\t')[1])
			delta = head1 - head2
			fixjump = true
