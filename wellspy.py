import datetime
import madspy
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

date0 = datetime.datetime(2000, 1, 1)

class SPoint:
	def __init__(self, filename, date0=date0):
		with open(filename, "r") as f:
			lines = f.readlines()
			f.close()
		self.wellname = lines[0].split()[0][1:]
		self.ddnames = lines[0].split("Cols:")[1].split()
		self.ddnames = map(lambda x: x[:-3], self.ddnames[2:])
		self.drawdowns = {}
		self.dts = []
		for ddname in self.ddnames:
			self.drawdowns[ddname] = []
		self.waterlevels = []
		for line in lines[1:]:
			splitline = line.split()
			self.dts.append(date0 + datetime.timedelta(days=float(splitline[0])))
			self.waterlevels.append(float(splitline[1]))
			for i in range(len(self.ddnames)):
				self.drawdowns[self.ddnames[i]].append(float(splitline[2 + i]))
	def getdrawdownnames(self):
		return self.ddnames
	def getwaterlevels(self):
		return self.waterlevels
	def getwaterleveltimes(self):
		return self.dts
	def getdrawdowns(self, ddname=[]):
		if ddname == []:
			return self.drawdowns
		else:
			return self.drawdowns[ddname]
	def getwellname(self):
		return self.wellname
	def getmaxdrawdown(self, ddname=[]):
		if ddname == []:
			return dict(map(lambda x: [x, self.getmaxdrawdown(x)], self.ddnames))
		else:
			return max(self.drawdowns[ddname])

def timedelta2days(td):
	return td.days + td.seconds / (24 * 3600.) + td.microseconds / (24 * 3600 * 1e6)

def datetime2datetimestring(dt):
	return datetime2datestring(dt) + " " + "_".join(map(lambda i: str(i).zfill(2), [dt.hour, dt.minute, dt.second]))

def datetime2datestring(dt):
	return "-".join(map(lambda i: str(i).zfill(2), [dt.year, dt.month, dt.day]))

def makewellsandmadsfiles(pumpinginfo, waterlevelinfo, rootfilename, date0=date0):
	pumpingparams = {}
	for wellname in pumpinginfo:
		pumpingparams[wellname] = {}
		pumpingparams[wellname]["Initial head"] = 1780
		pumpingparams[wellname]["Aquifer thickness"] = 1
		pumpingparams[wellname]["Permeability"] = "#K_" + wellname + "#"
		pumpingparams[wellname]["Storage coefficient"] = "#S_" + wellname + "#"
		pumpingparams[wellname]["Leakage coefficient"] = -10
	waterlevelparams = {}
	for wellname in waterlevelinfo:
		waterlevelparams[wellname] = {}
		waterlevelparams[wellname]["Initial head"] = "#ho_" + wellname + "#"
		waterlevelparams[wellname]["Temporal trend"] = "#c0_" + wellname + "#"
	#make the .tpl file
	makewellsfile(pumpinginfo, waterlevelinfo, pumpingparams, waterlevelparams, rootfilename + ".tpl", header="ptf #\n")
	#now make the mads data structure
	madsdata = {}
	madsdata["Problem"] = {"calibration": "null", "eval": 5000, "lmeigen": 1, "opt": "lm", "particles": "null", "sindx": 0.1, "single": "null", "ssr": "null"}
	madsdata["Command"] = "wells_hw " + rootfilename + " >" + rootfilename + "-wells_output"
	madsdata["Templates"] = [{"tmp1": {"tpl": rootfilename + ".tpl", "write": rootfilename + ".wells"}}]
	madsdata["Instructions"] = [{"ins1": {"ins": rootfilename + ".ins", "read": rootfilename + ".s_point"}}]
	madsdata["Parameters"] = []
	for wellname in pumpinginfo:
		madsdata["Parameters"].append({"K_" + wellname: {"init": 2.5, "log": False, "max": 8, "min": -6, "step": 0.1, "type": "opt", "init_max": 4, "init_min": -1}})
		madsdata["Parameters"].append({"S_" + wellname: {"init": -1.8, "log": False, "max": 0, "min": -6, "step": 0.1, "type": "opt", "init_max": -.5, "init_min": -4}})
	for wellname in waterlevelinfo:
		initwl = float(waterlevelinfo[wellname]["waterlevels"][0])
		madsdata["Parameters"].append({"ho_" + wellname: {"init": initwl, "log": False, "max": initwl + 20, "min": initwl - 20, "step": 0.1, "type": "opt"}})
		madsdata["Parameters"].append({"c0_" + wellname: {"init": -2, "log": False, "max": 2, "min": -10, "step": 0.1, "type": "opt", "init_max": .5, "init_min": -2}})
	madsdata["Observations"] = []
	insfile = open(rootfilename + ".ins", "w")
	insfile.write("pif @\n")
	#this loop adds the observations to the mads data structure and the ins file
	for wellname in waterlevelinfo:
		insfile.write("@" + wellname + "@\n")
		initwl = float(waterlevelinfo[wellname]["waterlevels"][0])
		for i in range(len(waterlevelinfo[wellname]["waterlevels"])):
			waterlevel = float(waterlevelinfo[wellname]["waterlevels"][i])
			dt = waterlevelinfo[wellname]["times"][i]
			obsname = "P" + datetime2datetimestring(dt).replace(" ", "_") + "_" + wellname
			#obsname = "P" + str(i)
			madsdata["Observations"].append({obsname: {"log": False, "max": waterlevel + 10, "min": waterlevel - 10, "target": waterlevel, "weight": 1}})
			time = round(timedelta2days(waterlevelinfo[wellname]["times"][i] - date0), 3)
			timestr = "%.3f" % time
			if time == int(time):
				time = int(time)
			insfile.write("@" + timestr + " @ !" + obsname + "!\n")
	insfile.close()
	my = madspy.MadsYaml(rootfilename + ".mads", data=madsdata)
	#dump the mads file
	my.dump(rootfilename + ".mads", forceproblemtop=True)

def makewellsfile(pumpinginfo, waterlevelinfo, pumpingparams, waterlevelparams, filename, header="", date0=date0):
	wellsfile = open(filename, "w")
	wellsfile.write(header)
	wellsfile.write("Problem name: test\n")
	wellsfile.write("Aquifer type: 1\n")
	wellsfile.write("Boundary type: 1 1 0 0 0\n")
	wellsfile.write("--- Number of wells: " + str(len(pumpinginfo)) + "\n")
	for wellname in pumpinginfo:
		numpumpingtimes = 0
		for i in range(len(pumpinginfo[wellname]["times"])):
			if i == 0 or pumpinginfo[wellname]["rates"][i] != pumpinginfo[wellname]["rates"][i - 1]:
				numpumpingtimes += 1
		wellsfile.write("\t".join(map(str, [wellname, pumpinginfo[wellname]["x"], pumpinginfo[wellname]["y"], pumpinginfo[wellname]["r"], numpumpingtimes])) + "\n")
		paramnames = ["Initial head", "Aquifer thickness", "Permeability", "Storage coefficient", "Leakage coefficient"]
		for paramname in paramnames:
			wellsfile.write(paramname + ": " + str(pumpingparams[wellname][paramname]) + "\n")
		for i in range(len(pumpinginfo[wellname]["times"])):
			if i == 0:
				wellsfile.write("\t".join(map(str, [timedelta2days(pumpinginfo[wellname]["times"][i] - date0), pumpinginfo[wellname]["rates"][i]])) + "\n")
			elif pumpinginfo[wellname]["rates"][i] != pumpinginfo[wellname]["rates"][i - 1]:
				wellsfile.write("\t".join(map(str, [timedelta2days(pumpinginfo[wellname]["times"][i] - date0), pumpinginfo[wellname]["rates"][i]])) + "\n")
	wellsfile.write("--- Number of points: " + str(len(waterlevelinfo)) + "\n")
	for wellname in waterlevelinfo:
		wellsfile.write("\t".join(map(str, [wellname, waterlevelinfo[wellname]["x"], waterlevelinfo[wellname]["y"], len(waterlevelinfo[wellname]["times"])])) + "\n")
		paramnames = ["Initial head", "Temporal trend"]
		for paramname in paramnames:
			wellsfile.write(paramname + ": " + str(waterlevelparams[wellname][paramname]) + "\n")
		for i in range(len(waterlevelinfo[wellname]["times"])):
			wellsfile.write(str(timedelta2days(waterlevelinfo[wellname]["times"][i] - date0)) + "\n")
	wellsfile.close()

def plotspointfile(spointfilename, waterlevelinfo, date0=date0, title=[], months=[1, 5, 9]):
	spoint = SPoint(spointfilename)
	f, axarr = plt.subplots(2, sharex=True)
	wellname = spoint.getwellname()
	box = axarr[0].get_position()
	axarr[0].set_position([box.x0, box.y0, box.width * 0.88, box.height])
	box = axarr[1].get_position()
	axarr[1].set_position([box.x0, box.y0, box.width * 0.88, box.height])
	axarr[0].plot(waterlevelinfo[wellname]["times"], waterlevelinfo[wellname]["waterlevels"], "k.")
	spointwaterleveltimes = spoint.getwaterleveltimes()
	spointwaterlevels = spoint.getwaterlevels()
	axarr[0].plot(spointwaterleveltimes, spointwaterlevels, 'r', linewidth=2)
	if title != []:
		axarr[0].set_title(title)
	axarr[0].set_ylabel("Head [m]")
	ddnames = spoint.getdrawdownnames()
	drawdowns = spoint.getdrawdowns()
	if ddnames[ddnames=="trend"] and ddnames[ddnames=="total"]:
		mycmap=plt.cm.gist_ncar(np.linspace(0,1,len(ddnames)-1)) # subtract 1 for total and trend (no plot white)
	else:
		mycmap=plt.cm.gist_ncar(np.linspace(0,1,len(ddnames)+1)) # no plot white
	ddnames_labels = map(lambda ddname:ddname.replace('0',''), ddnames) # remove 0 from well names
	ctr_col = 0 # iterator for custom colormap
	mod_dd = [] # used to recalculate total
	# First plot trend
	print(ddnames)
	for i in range(len(ddnames)):
		mindd = min(drawdowns[ddnames[i]])        
		if ddnames[i] == "trend":
			axarr[1].plot(spointwaterleveltimes, map(lambda dd: dd - mindd, drawdowns[ddnames[i]]), label = "trend", c="k", ls="--", linewidth=1.5)
			mod_dd.append(map(lambda dd: dd - mindd, drawdowns[ddnames[i]]))
	# Then plot wells
	for i in range(len(ddnames)):
		mindd = min(drawdowns[ddnames[i]])
		if ddnames[i] != "total" and ddnames[i] != "trend":
			mod_dd.append(map(lambda dd: dd - mindd, drawdowns[ddnames[i]]))
			axarr[1].plot(spointwaterleveltimes, map(lambda dd: dd - mindd, drawdowns[ddnames[i]]), c=mycmap[ctr_col], label = ddnames_labels[i], linewidth=1.5)
			ctr_col += 1
	# Then plot total
	# PROBLEM: Sometimes individual drawdowns are greater than total!
	# SOLUTION 1: calculate a new total, which is done in the for loop above (mod_dd) 
        total_dd = []
	for i in range(len(mod_dd[0])):
		mysum = 0
		for j in range(len(mod_dd)):
			mysum += mod_dd[j][i]
		total_dd.append(mysum)
        # SOLUTION 2: TODO, would need to reevaluate mindd (e.g. dont calc mindd using times before observation begins) 
	axarr[1].plot(spointwaterleveltimes, total_dd, label = "total", c="r", linewidth=1.75)
	axarr[1].set_ylabel("Drawdown [m]")
	axarr[1].xaxis.set_major_locator(matplotlib.dates.MonthLocator(bymonth=months))
	axarr[1].xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%b %Y"))
	ymin, ymax = plt.ylim()
	xmin, xmax = plt.xlim()
	plt.axis([xmin, xmax, ymax, ymin])
	handles, labels = axarr[1].get_legend_handles_labels()
	# sort both labels and handles by labels
	labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
	axarr[1].legend(handles, labels, loc=4, bbox_to_anchor=(1.28, -0.05))
	# rotate x axis
	labels = axarr[1].get_xticklabels() 
	for label in labels: 
		label.set_rotation(30) 
